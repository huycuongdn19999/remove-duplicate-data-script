
const mongoose = require('mongoose')
const StationAuto = {
  name: 'station-autos',
  schema: new mongoose.Schema({
    key: String,
    name: String,
    measuringList: Object,
    dataFrequency: Number
  })
}
module.exports = StationAuto


