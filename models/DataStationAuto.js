const mongoose = require('mongoose')


const prefix = 'data-station-'
// enable logging collection methods + arguments to the console

function createDataModel(tableWithPrefix, conn) {
  if (conn.models && conn.models[tableWithPrefix]) {
    return conn.models[tableWithPrefix]
  } else {
    let schema = new mongoose.Schema({
      receivedAt: { type: Date, default: Date.now },
      measuringLogs: Object,
      createdAt: { type: Date, default: Date.now },
      updatedAt: { type: Date, default: Date.now }
    })
    schema.index({ receivedAt: -1 })
    return conn.model(tableWithPrefix, schema)
  }
}
module.exports = createDataModel