# Remove duplicate scripts

Developed by VAS Developer

## Installation

```bash
git clone https://gitlab.com/huycuongdn19999/remove-duplicate-data-scriptstarter-boilerplate/next-starter.git

cd remove-duplicate-data-script
yarn install
```

## How to run

Copy `.env-example` to `.env` and replace environments

```bash
# database's information
DATABASE_NAME ="YOUR_DATABASE_NAME"
DATABASE_ADDRESS ="YOUR_DATABASE_ADDRESS"
DATABASE_USERNAME ="YOUR_DATABASE_USERNAME"
DATABASE_PASSWORD ="YOUR_DATABASE_PASSWORD"
DATABASE_PORT ="YOUR_DATABASE_PORT"
```

Run:

```bash
bash removeDuplicates.sh
```

or:

```bash
sh removeDuplicates.sh
```

After execute the bash file, the tool will automatically find and remove duplicated record in the database
