
const { assignDao, createConnection } = require('./shared/mongoUtils/connect')
const createDataModel = require('./models/DataStationAuto')
const dbInfo = require('./config');
const { model } = require('mongoose');
const fs = require('fs')


let mongoDbConnect
let conn

const initConnection = async () => {
  console.log('🚀 Step 1: Init connection')
  conn = await createConnection(dbInfo)
  console.log(dbInfo, '==dbInfo===')
}

const getListDataStationKeyModel = async () => {
  console.log('🚀 Step 2: Get list station key')

  const modelName = 'station-autos'
  const dataModel = createDataModel(modelName, conn)
  const objectKeys = await dataModel.find().select({ key: 1, _id: 0 }).lean()
  const result = await objectKeys.map(item => item.key)
  return result
}

const main = async () => {
  await initConnection()
  const listModelName = await getListDataStationKeyModel()

  const workerPromise = listModelName.map(async stationKey => {

    let modelName = `data-station-${stationKey}`
    let duplicates = { db: modelName, data: [] }
    const dataModel = createDataModel(modelName, conn)

    console.log(` ❤️  Step 3: query data ${modelName}`)
    const list = await dataModel.aggregate([
      {
        $addFields: { dateWithNoTime: { "$dateToString": { "format": "%Y-%m-%d-%H-%M", "date": "$receivedAt" } } }
      },
      {
        $group: {
          _id: '$dateWithNoTime',
          dupsId: { "$addToSet": "$_id" },
          count: { "$sum": 1 }
        }
      },
      {
        $match: {
          count: { "$gt": 1 }
        }
      }
    ]
    )
      .allowDiskUse(true)
    // console.log(list.length)
    // fs.writeFileSync('/Users/sevenlure/self_study/script/testne', JSON.stringify(list))
    console.log(` 🥰 Step 4: remove duplicate ids in query result ${modelName}`)
    list.forEach(function (doc) {
      doc.dupsId.shift();
      doc.dupsId.forEach(function (dupId) {
        duplicates.data.push(dupId);
      }
      )
    })

    console.log(` 🥳 Step 5: delete ${duplicates.data.length} record of ${modelName}`)
    await dataModel.remove({ _id: { $in: duplicates.data } })
  })

  await Promise.all(workerPromise)


}

main().then(() => process.exit(1))