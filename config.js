require('dotenv').config()
const DB_INFO = {
  name: process.env.DATABASE_NAME,
  address: process.env.DATABASE_ADDRESS,
  user: process.env.DATABASE_USERNAME,
  pwd: process.env.DATABASE_PASSWORD,
  port: process.env.DATABASE_PORT,
}
module.exports = DB_INFO